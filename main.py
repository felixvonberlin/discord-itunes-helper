#!/usr/bin/env python3

# Discord-iTunes Helper
# Copyright (C) 2019 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.
# Discord is a trademark, which may is registered in the U.S. and other countries.

# === import statements ===
from pypresence  import Presence
from subprocess  import Popen, PIPE
from time        import sleep, time
from tkinter     import *

import tkinter.font as TkFont
import threading
import asyncio
import os
import json

# === constants ===

ERROR = ["-1", "-2"]
CID = "1234567890"

# === main programm ===

class MainWindow(Frame):

	def updateScreen(self, title, artist, paused):
		if title == artist == None:
			self.SONG["text"] = "Not playing"
		else:
			self.SONG["text"] = artist + " - " + title + (" (paused)" if paused else "")

	def stop(self):
		self.running = False
		self.quit()

	def musicService(self):
		asyncio.set_event_loop(self.loop)
		rpc = None
		try:
			while self.running:
				process = Popen(
					["osascript", "-l", "JavaScript", "MusicHelper.applescript"],
					stdout=PIPE)
				(out, err) = process.communicate()
				process.wait()
				out = str(out, encoding="utf8").strip()
				if not(out in ERROR):
					try:
						j = json.loads(out)
					except:
						continue				
					if rpc == None:
						rpc = Presence(CID)
						rpc.connect()
					try:
						playTime = int(j['duration']) - int(j['position'])
					except:
						playTime = 0;
					endtime = (time()+playTime if j['playbackstate'] == "playing" else None)
					rpc.update(state=j['artist'],
					details=j['name'] + (" (paused)" if j['playbackstate'] != "playing" else ""),
					end=endtime if j['duration'] != 0 else None)
					self.updateScreen(j['name'], j['artist'], not(j['playbackstate'] == "playing"))
					if playTime < 10:
						sleep(2)
						continue;
				else:
					if out == "-2":
						if rpc != None:
							rpc.close()
						os.system("osascript -e 'display notification \"Service has stopped.\" with title \"Discord Music Helper\"'")
						break;
					if out == "-1":
						self.updateScreen(None, None, False)
						if rpc == None:
							rpc = Presence(CID)
							rpc.connect()
						rpc.clear()
				sleep(5)
		except KeyboardInterrupt:
			if rpc != None:
				os.system("osascript -e 'display notification \"Service has stopped.\" with title \"Discord Music Helper\"'")
			rpc.close();

	def createWidgets(self):
		self.PLAYING = Label(self.master)
		self.PLAYING['text'] = "Now playing:"
		self.PLAYING.grid(column=0, row=0, columnspan=2, padx=(16,32), pady=(16,0))
		self.SONG = Label(self.master)
		self.SONG["text"] = "Not playing"
		self.SONG.grid(column=0, row=1, columnspan=2, pady=32, padx=32)

		self.QUIT = Button(self.master)
		self.QUIT["text"] = "Stop Script"
		self.QUIT["command"] =  self.stop
		self.QUIT.grid(column=0, row=2, padx=16, pady=16, sticky="W")
	
	def __init__(self, master = None):
		super().__init__(master)
		self.master = master
		master.columnconfigure(0, weight=1)
		master.rowconfigure(0, weight=1)
		master.title("iTUnes Discord Helper")
		master.resizable(False, False)
		master.geometry('500x180') 

		self.createWidgets()
		self.loop = asyncio.new_event_loop()
		self.running = True
		self.thread = threading.Thread(target=self.musicService)
		self.thread.start()

if __name__ == '__main__':
	tk = Tk()
	app = MainWindow(master=tk)
	app.mainloop()




#!/usr/bin/env python3

# Discord-iTunes Helper (curses)
# Copyright (C) 2020 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.
# Discord is a trademark, which may is registered in the U.S. and other countries.

# === import statements ===
from pypresence  import Presence, InvalidID
from subprocess  import Popen, PIPE
from time        import sleep, time
import curses
import locale
import threading
import asyncio
import os
import json

# === CONSTANTS ===
ERROR = ["-1", "-2"]
CID = "123456789"

# === fixed texts ===
TEXT_TITLE_BAR = "  Music Discord Helper (noX)  "
TEXT_HELP_TEXT = "q: quit  p: pause/resume"
TEXT_QUIT_1    = "  QUITTING  "
TEXT_QUIT_2    = " Waiting for iTunes Plugin... "
TEXT_UPDATING  = "updating ..."

# === usefull helper methods ===

def get_human_time(seconds):
    m, s = divmod(seconds, 60)
    return (str(m) if m > 0 else "") + "m " + str(s) + "s";
 
def get_centered_coord(txt):
    return int((curses.COLS - len(txt))/2)

def print_centered(scr, txt, y, *args):
    if len(txt) > curses.COLS:
      txt = txt[0:curses.COLS-1]
    scr.addstr(y, get_centered_coord(txt), txt, *args)

def initUI(stdscr):
    stdscr.clear()
    curses.init_pair(1,  curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(2,  curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(10, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.curs_set(0)

def drawUI(stdscr):
    stdscr.bkgdset(curses.color_pair(2)) 
    stdscr.clear()
    print_centered(stdscr, " " * len(TEXT_TITLE_BAR), 0, curses.color_pair(1))
    print_centered(stdscr, TEXT_TITLE_BAR, 1, curses.color_pair(1))
    print_centered(stdscr, " " * len(TEXT_TITLE_BAR), 2, curses.color_pair(1))

    stdscr.addstr(curses.LINES-1, 0, (" " * (curses.COLS-1)), curses.color_pair(10))
    stdscr.addstr(curses.LINES-1, 0, TEXT_HELP_TEXT, curses.color_pair(10))
    stdscr.refresh()

def drawState(stdscr, state, artist = "", title = "", time = 0):
    drawUI(stdscr);
    if state == "notrunning":
        print_centered(stdscr, "iTunes or/nor Discord are not running.", int(curses.LINES/2))

    if state == "idle":
        print_centered(stdscr, "iTunes is idle.", int(curses.LINES/2))
    
    if state == "paused":
        print_centered(stdscr, "PAUSE", int(curses.LINES/2)-1)
        print_centered(stdscr, "press p to continue", int(curses.LINES/2)+1)

    if state == "active":
        print_centered(stdscr, "iTunes is playing", int(curses.LINES/2)-2, curses.color_pair(1))
        print_centered(stdscr, artist + " - " + title, int(curses.LINES/2))          
        print_centered(stdscr, get_human_time(time) + " remaining", int(curses.LINES/2)+1)          

    if state == "error":
        print_centered(stdscr, "Something went wrong!", int(curses.LINES/2), curses.color_pair(1))
    stdscr.refresh()

def drawUpdating(stdscr):
    stdscr.addstr(curses.LINES - 2, curses.COLS -(len(TEXT_UPDATING)+1), TEXT_UPDATING)
    stdscr.refresh()

def musicService(loop, scr):
    rpc = None
    asyncio.set_event_loop(loop)
    while getattr(threading.currentThread(), "running", True):
        process = Popen(["osascript", "-l", "JavaScript", "MusicHelper.applescript"],stdout=PIPE)
        (out, err) = process.communicate()
        process.wait()
        out = str(out, encoding="utf8").strip()
        if not(out in ERROR):
            try:
                j = json.loads(out)
                if rpc == None:                                                 
                    rpc = Presence(CID)                                         
                    rpc.connect() 
                if getattr(threading.currentThread(), "paused", False):
                    drawState(scr,"paused")
                    rpc.clear()
                    continue;
                try:
                    playTime = int(j['duration']) - int(j['position'])
                except:
                    playTime = 0;
                endtime = (time()+playTime if j['playbackstate'] == "playing" else None)
                drawState(scr, "active", j['artist'],
                  j['name'] + (" (paused)" if j['playbackstate'] != "playing" else ""),
                  (playTime if j['playbackstate'] == "playing" else 0))
                rpc.update(state=j['artist'],
                  details=j['name'] + (" (paused)" if j['playbackstate'] != "playing" else ""),
                  end=endtime if j['duration'] != 0 else None)
            except InvalidID:
                drawState(scr, "error")
                rpc = None
                continue;
            except ConnectionRefusedError:
                drawState(scr, "notrunning")
                rpc = None
                continue;
            except Exception as e:
                drawState(scr, "error")
                print_centered(scr, str(e), 10)
                scr.refresh()
                continue;
        else:
            if out == "-2":
                if rpc != None:
                    rpc.close()
                drawState(scr, "notrunning")
                break;
            if out == "-1":
                drawState(scr, "idle")
                rpc.clear()
        sleep(3)


# === main method ===
def main(stdscr):
    initUI(stdscr);
    drawUI(stdscr);
 
    loop = asyncio.new_event_loop() 
    thread = threading.Thread(target=musicService, args=(loop, stdscr,))
    thread.start()

    pk = ""
    while True:
        if pk == "q":
            stdscr.clear()
            print_centered(stdscr, " " * int(len(TEXT_QUIT_2)), int(curses.LINES*2/5)    , curses.color_pair(1))
            print_centered(stdscr, " " * int(len(TEXT_QUIT_2)), int(curses.LINES*2/5) + 1, curses.color_pair(1))
            print_centered(stdscr, TEXT_QUIT_1                , int(curses.LINES*2/5) + 1, curses.color_pair(1))
            print_centered(stdscr, " " * int(len(TEXT_QUIT_2)), int(curses.LINES*2/5) + 2, curses.color_pair(1))
            print_centered(stdscr, TEXT_QUIT_2                , int(curses.LINES*2/5) + 3, curses.color_pair(1))
            stdscr.refresh()
            thread.running = False
            thread.join()
            break;
        if pk == "p":
            thread.paused = not(getattr(thread, "paused", False))
        pk = stdscr.getkey()


curses.wrapper(main)

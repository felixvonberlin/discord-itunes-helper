#!/usr/bin/env python3

# Discord-iTunes Helper
# Copyright (C) 2019 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.
# Discord is a trademark, which may is registered in the U.S. and other countries.

# ##### installer script #####

# === import statements ===

import os
import pkgutil
import time
import sys
from subprocess import Popen, PIPE
from shutil import copyfile


def printC(*args, end="\n"):
  for a in args:
    for x in a:
      for y in x:
        print(y, end="")
        sys.stdout.flush()
        time.sleep(0.01)
  print(end, end="")

# === main progam ===

MODULES = ["pypresence", "subprocess", "json"]
userHome = os.path.expanduser("~")
scriptHome = userHome + "/Applications/MusicHelper"

printC("\n")
printC("################################################################################")
printC("###  iTunes Discord Helper Script  #############################################")
printC("###  FAvO Apps                     #############################################")
printC("################################################################################")

printC("""
iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.
Discord is a trademark, which may is registered in the U.S. and other countries.
""")
printC("""\n
    iTunes Dicord Helper Script  Copyright (C) 2019 Felix v. Oertzen
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions.
\n""")
printC("Install the helper script to " + scriptHome + "? [y/N] ", end="")
if input("") == "y":
  printC("Checking for modules...", end="")
  installedMods = []
  for mod in pkgutil.iter_modules():
    installedMods.append(mod.name)
  missingMods = set(MODULES) - set(installedMods)
  if len(missingMods) != 0:
    printC("\t\t[FAILED]")
    printC("The modules " + " ".join(missingMods) + " are missing...\nInstalling...", end="")
    for mm in missingMods:
      process = Popen(
        ["pip3", "install", mm], stdout=PIPE)
      (out, err) = process.communicate()
      process.wait()
    printC("\t\t\t[OK]")
  else:
    printC("\t\t[OK]") 
  printC("Copying files ...", end="")
  cwd = os.getcwd()
  files = ["/main.py","/MusicHelper.applescript", "/console.py"]
  if not(os.path.exists(scriptHome)):
    os.mkdir(scriptHome)
  for file in files:
    copyfile(cwd+file,scriptHome+file)
  printC("\t\t[OK]")
  
  # replace the discord app ID
  printC("Please enter your Client ID (see README.md):", end="")
  dID = input("")
  printC("Changing ID...", end="")
  for f in files:
    mainFile = ""
    if not(f.endswith(".py")):
      continue;
    with open(cwd + f, "r", encoding='utf-8') as main:
      for line in main:
        if line.startswith("CID = "):
            mainFile = mainFile + "CID = \"" + dID + "\"\n"
        else:
            mainFile = mainFile + line
      with open(scriptHome + f, "w") as main:
        for line in mainFile:
          main.write(line)
  printC("\t\t\t[OK]")
else:
  printC("Aborting...")
printC("Finishing \t\t\t[OK]")
printC("")




